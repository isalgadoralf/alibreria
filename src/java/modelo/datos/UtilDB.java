/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.datos;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.text.SimpleDateFormat;


/**
 *
 * @author Rafael
 */
public class UtilDB {

    public static SimpleDateFormat FormatoFecha;
    public static SimpleDateFormat FormatoHora = new SimpleDateFormat("HH:mm:ss");

    public static boolean esAuxiliar(String campo) {

        return campo.substring(0, 1).equals("_");
    }
   

    public static final String getConsultaInsertar(Object obj) {
        Class objeto = obj.getClass();

        String consulta = "INSERT INTO " + objeto.getSimpleName().toLowerCase() + " ( ";
        String valores = " VALUES ( ";
        Field[] fields = objeto.getDeclaredFields();

        for (Field f : fields) {
            f.setAccessible(true);

            if (!UtilDB.isPrimaria(f.getName(), objeto.getSimpleName()) && !UtilDB.esAuxiliar(f.getName())) {
                consulta = consulta + f.getName() + ", ";

                if (f.getType().getSimpleName().equals("String") || f.getType().getSimpleName().equals("Date") || f.getType().getSimpleName().equals("Time")) {
                    if (f.getType().getSimpleName().equals("Date")) {
                        try {
                            FormatoFecha = new SimpleDateFormat("yyyy-MM-dd");

                            Date fecha = new Date();
                            String fs = FormatoFecha.format(fecha);

                            if (f.getName().equalsIgnoreCase("fechaI") || f.getName().equalsIgnoreCase("fecha")) {
                                if (f.get(obj) != null) {
                                    valores = valores + "'" + fs + "'" + ", ";
                                } else {
                                    valores = valores + "'" + "'" + ", ";
                                }
                            }

                        } catch (IllegalArgumentException ex) {
                            Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IllegalAccessException ex) {
                            Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    } else {
                        if (f.getType().getSimpleName().equals("Time")) {
                            try {
                                valores = valores + "'" + f.get(obj).toString() + "'" + ", ";
                            } catch (IllegalArgumentException ex) {
                                Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IllegalAccessException ex) {
                                Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else {
                            try {

                                if (f.get(obj) != null) {
                                    valores = valores + "'" + f.get(obj).toString() + "'" + ", ";
                                } else {
                                    valores = valores + "'" + "'" + ", ";
                                }
                            } catch (IllegalArgumentException ex) {
                                Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IllegalAccessException ex) {
                                Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }

                    }

                } else {
                    try {
                        if (f.get(obj) != null) {
                            valores = valores + f.get(obj).toString() + ", ";
                        } else {
                            valores = valores + -1 + ", ";
                        }
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
            f.setAccessible(false);

        }

        valores = valores.substring(0, valores.length() - 2);
        valores = valores + ")";
        consulta = consulta.substring(0, consulta.length() - 2);
        consulta = consulta + " )" + valores + ";";

        return consulta;
    }

    public static final String getConsultaInsertarID(Object obj) {
        Class objeto = obj.getClass();

        String consulta = "INSERT INTO " + objeto.getSimpleName().toLowerCase() + " ( ";
        String valores = " VALUES ( ";
        Field[] fields = objeto.getDeclaredFields();

        for (Field f : fields) {
            f.setAccessible(true);
            if (!UtilDB.esAuxiliar(f.getName())) {
                consulta = consulta + f.getName() + ", ";

                if (f.getType().getSimpleName().equals("String") || f.getType().getSimpleName().equals("Date") || f.getType().getSimpleName().equals("Time")) {
                    if (f.getType().getSimpleName().equals("Date")) {
                        try {
                            FormatoFecha = new SimpleDateFormat("yyyy-MM-dd");

                            String fs = FormatoFecha.format(f.get(obj));

                            valores = valores + "'" + fs + "'" + ", ";

                        } catch (IllegalArgumentException ex) {
                            Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IllegalAccessException ex) {
                            Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    } else {
                        if (f.getType().getSimpleName().equals("Time")) {
                            try {
                                valores = valores + "'" + f.get(obj).toString() + "'" + ", ";
                            } catch (IllegalArgumentException ex) {
                                Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IllegalAccessException ex) {
                                Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else {
                            try {

                                if (f.get(obj) != null) {
                                    valores = valores + "'" + f.get(obj).toString() + "'" + ", ";
                                } else {
                                    valores = valores + "'" + "'" + ", ";
                                }
                            } catch (IllegalArgumentException ex) {
                                Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IllegalAccessException ex) {
                                Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }

                    }
                } else {
                    try {
                        if (f.get(obj) != null) {
                            valores = valores + f.get(obj).toString() + ", ";
                        } else {
                            valores = valores + -1 + ", ";
                        }
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

            }
            f.setAccessible(false);
        }

        valores = valores.substring(0, valores.length() - 2);
        valores = valores + ")";
        consulta = consulta.substring(0, consulta.length() - 2);
        consulta = consulta + " )" + valores + ";";

        return consulta;
    }

    public static void main(String[] args) {

        //  System.out.println(UtilDB.getConsultaTodos(po));
        //   System.out.println(UtilDB.getObjectoID(e));
        //   System.out.println(UtilDB.getConsultaUpdate(e));
        //  System.out.println(UtilDB.getObjectCampo(u, "login",u.getLogin()));
//        Deitma aux = new Deitma(15.0, 10, 15, 20.0);
//        String au = UtilDB.getConsultaUpdateDetalle(aux);
//        System.out.println(au);
    }

    public static boolean isPrimaria(String campo) {
        if (campo.length() <= 3) {
            return false;
        }
        String valor = campo.substring(campo.length() - 3, campo.length());
        if (valor.substring(valor.length() - 2, valor.length()).equalsIgnoreCase("Id")) {
            if (valor.substring(0, valor.length() - 2).equals("_")) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }

    }

    public static boolean isPrimaria(String campo, String nombreTabla) {
        if (campo.length() <= 3) {
            return false;
        }
        String sub = nombreTabla + "ID";
        return campo.equalsIgnoreCase(sub);

    }

    public static boolean isPrimariaOriginal(String campo) {

        String sub = campo.substring(campo.length() - 2, campo.length());
        System.out.println(sub);
        return sub.equals("ID");

    }

    public static final String getConsultaTodos(Object obj) {
        Class objeto = obj.getClass();
        String consulta = "SELECT * FROM " + objeto.getSimpleName().toLowerCase() + ";";
        return consulta;
    }

    public static final String getTodosProductosAlmacen(int idalmacen) {
        String consulta = "SELECT p.productoID,p.descripcion as nombre,c.descripcion,  i.actual FROM producto p,categoria c inner join detaproalma i"
                + " where p.productoID = i.producto_id  and i.almacen_id = " + idalmacen + " and c.categoriaID = p.categoria_id;";
        return consulta;
    }

    public static final String getTodosProductosIngreso(int idalmacen) {
        //String consulta = "SELECT p.productoID,p.categoria_id,p.umedida_id ,p.descripcion,p.observacion FROM producto p inner join detaproing i where p.productoID = i.producto_id;";
        String consulta = "SELECT p.productoID,p.categoria_id,p.umedida_id ,p.descripcion,p.observacion FROM  detaproing i  inner join  producto p ON p.productoID = i.producto_id INNER JOIN detaproalma de ON p.productoID = de.producto_id AND almacen_id = " + idalmacen;

        return consulta;
    }

    public static final String getTodosStockDescripcion(String descripcion, int idalmacen) {
        String consulta = "SELECT p.productoID,p.descripcion as nombre,c.descripcion,  i.actual FROM producto p,categoria c inner join detaproalma i"
                + " where p.productoID = i.producto_id  and i.almacen_id = " + idalmacen + " and p.descripcion like '%" + descripcion + "%' and c.categoriaID = p.categoria_id;";
        return consulta;
    }

    public static final String getTodosIngresoDescripcion(String descripcion, int idalmacen) {
        String consulta = "SELECT i.ingresoID, i.fechaI, i.horaI, p.descripcion,u.abreviatura, d.cantidad,pr.nombre, pr.apellidos FROM ingreso i INNER JOIN detaproing d\n"
                + " ON i.ingresoID = d.ingreso_id INNER JOIN producto p ON d.producto_id = p.productoID   INNER JOIN umedida u ON p.umedida_id = u.umedidaID"
                + " INNER JOIN proveedor pr ON pr.proveedorID = i.proveedor_id"
                + " AND p.descripcion like '%" + descripcion + "%' AND i.almacen_id = " + idalmacen + ";";
        return consulta;
    }

    public static final String getTodosEgresoDescripcion(String descripcion, int idalmacen) {
        String consulta = "SELECT i.egresoID, i.fechaI, i.horaI,pr.descripcion,um.abreviatura,o.nombre,o.apellidos ,n.nombre AS nnombre,it.nombre AS inombre, d.cantidad FROM egreso i INNER JOIN nivel n "
                + "ON i.nivel_id =n.nivelID INNER JOIN detaprosa d ON d.egreso_id = i.egresoID INNER JOIN item it "
                + "ON d.item_id = it.itemID INNER JOIN obrero o ON o.obreroID = i.obrero_id INNER JOIN  producto pr "
                + "ON pr.productoID = d.producto_id INNER JOIN umedida um ON  um.umedidaID = pr.umedida_id "
                + "AND pr.descripcion like '%" + descripcion + "%' AND i.almacen_id = " + idalmacen + ";";
        return consulta;
    }

    public static final String getConsultaUltimo(Object obj) {
        Class objeto = obj.getClass();
        String id = obj.getClass().getSimpleName().toLowerCase() + "ID";
        //   "SELECT * FROM umedida p order by umedidaID desc limit 1"
        String consulta = "SELECT * FROM " + objeto.getSimpleName().toLowerCase() + " ORDER BY " + id + " desc limit 1;";
        return consulta;
    }

    public static final String getObjectoID(Object obj) {
        try {
            String tabla = obj.getClass().getSimpleName().toLowerCase();
            Class objeto = obj.getClass();
            // String id = obj.getClass().getSimpleName() + "ID";
            String consulta = "SELECT * FROM " + tabla + " WHERE " + tabla + "ID = ";
            //Field[] fields = 
            tabla = tabla.toLowerCase() + "ID";
            Field f = objeto.getDeclaredField(tabla);
            f.setAccessible(true);
            String valores = "";

            if (f.get(obj) != null) {
                valores = valores + f.get(obj).toString();
            } else {
                valores = valores + -1;
            }
            consulta = consulta + valores + ";";

            f.setAccessible(false);
            return consulta;
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchFieldException ex) {
            Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static final String getConsultaUpdate(Object obj) {
        Class objeto = obj.getClass();
        String condicion = " WHERE ";
        String consulta = "UPDATE " + objeto.getSimpleName().toLowerCase() + " SET ";
        String valores = "";
        Field[] fields = objeto.getDeclaredFields();

        for (Field f : fields) {
            f.setAccessible(true);
            if (!UtilDB.isPrimaria(f.getName(), objeto.getSimpleName()) && !UtilDB.esAuxiliar(f.getName())) {
                consulta = consulta + f.getName() + " = ";
                valores = "";
                if (f.getType().getSimpleName().equals("String") || f.getType().getSimpleName().equals("Date") || f.getType().getSimpleName().equals("Time")) {
                    try {
                        if (f.get(obj) != null) {
                            valores = valores + "'" + f.get(obj).toString() + "'" + ", ";
                        } else {
                            valores = valores + "'" + "'" + ", ";
                        }
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        if (f.get(obj) != null) {
                            valores = valores + f.get(obj).toString() + ", ";
                        } else {
                            valores = valores + -1 + ", ";
                        }
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                consulta = consulta + valores;
            } else {
                try {
                    condicion = condicion + f.getName() + " = " + f.get(obj).toString();

                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            f.setAccessible(false);
        }

        consulta = consulta.substring(0, consulta.length() - 2);
        consulta = consulta + condicion + ";";

        return consulta;
    }

    public static final String getConsultaUpdateDetalle(Object obj) {
        Class objeto = obj.getClass();
        boolean sw = true;
        String condicion = " WHERE ";
        String consulta = "UPDATE " + objeto.getSimpleName().toLowerCase() + " SET ";
        String valores = "";
        Field[] fields = objeto.getDeclaredFields();

        for (Field f : fields) {
            f.setAccessible(true);
            if (!UtilDB.isPrimariaOriginal(f.getName()) && !UtilDB.esAuxiliar(f.getName())) {
                consulta = consulta + f.getName() + " = ";
                valores = "";
                if (f.getType().getSimpleName().equals("String") || f.getType().getSimpleName().equals("Date") || f.getType().getSimpleName().equals("Time")) {
                    try {
                        if (f.get(obj) != null) {
                            valores = valores + "'" + f.get(obj).toString() + "'" + ", ";
                        } else {
                            valores = valores + "'" + "'" + ", ";
                        }
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        if (f.get(obj) != null) {
                            valores = valores + f.get(obj).toString() + ", ";
                        } else {
                            valores = valores + -1 + ", ";
                        }
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                consulta = consulta + valores;
            } else {
                try {
                    if (sw) {
                        sw = !sw;
                        condicion = condicion + f.getName() + " = " + f.get(obj).toString();
                    } else {
                        condicion = condicion + " AND ";
                        condicion = condicion + f.getName() + " = " + f.get(obj).toString();
                    }

                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            f.setAccessible(false);
        }

        consulta = consulta.substring(0, consulta.length() - 2);
        consulta = consulta + condicion + ";";

        return consulta;
    }

    public static final String getObjectCampo(Object obj, String campo, Object data) {
        try {
            String tabla = obj.getClass().getSimpleName().toLowerCase();
            Class objeto = obj.getClass();
            String consulta = "SELECT * FROM " + tabla + " WHERE " + campo + " = ";

            Field[] fields = objeto.getDeclaredFields();
            Field f = fields[0];
            f.setAccessible(true);
            String valores = "";

            if (data.getClass().getSimpleName().equals("String") || data.getClass().getSimpleName().equals("Date") || f.getType().getSimpleName().equals("Time")) {
                consulta = consulta + "'" + data + "'";
            } else {
                if (data.getClass().getSimpleName().equals("Integer")) {
                    consulta = consulta + data;
                }

            }
            f.setAccessible(false);
            consulta = consulta + valores + ";";
            return consulta;
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(UtilDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public static String getItems(int materialID) {
        String aux = "SELECT item.descripcion,item. precio,item.itemID,item.umedidaID,item.categoriaID,item.color,item.tipo\n"
                + "FROM item\n"
                + "JOIN deitma\n"
                + "ON item.itemID=deitma.itemID\n"
                + "where deitma.materialesID = " + materialID;
        return aux;
    }

    public static String getMateriales(int intemID) {
        String aux = "SELECT deitma.cantidad,deitma.materialesID,deitma.itemID,deitma.stotal\n"
                + "FROM deitma \n"
                + "JOIN item\n"
                + "ON item.itemID=deitma.itemID\n"
                + "where deitma.itemID = " + intemID;
        return aux;
    }

    public static String getItemsMo(int materialID) {
        String aux = "SELECT item.descripcion,item. precio,item.itemID,item.umedidaID,item.categoriaID,item.color,item.tipo\n"
                + "FROM item\n"
                + "JOIN deitmo\n"
                + "ON item.itemID=deitmo.itemID\n"
                + "where deitmo.mobraID =  " + materialID;
        return aux;
    }

    public static String getMobra(int intemID) {
        String aux = "SELECT deitmo.cantidad,deitmo.mobraID,deitmo.itemID,deitmo.stotal\n"
                + "FROM deitmo \n"
                + "JOIN item\n"
                + "ON item.itemID=deitmo.itemID\n"
                + "where deitmo.itemID = " + intemID;
        return aux;
    }
     public static String getItemsEq(int materialID) {
        String aux = "SELECT item.descripcion,item. precio,item.itemID,item.umedidaID,item.categoriaID,item.color,item.tipo\n"
                + "FROM item\n"
                + "JOIN deitmo\n"
                + "ON item.itemID=deitmo.itemID\n"
                + "where deitmo.mobraID =  " + materialID;
        return aux;
    }

    public static String getMequi(int intemID) {
        String aux = "SELECT deitmo.cantidad,deitmo.mobraID,deitmo.itemID,deitmo.stotal\n"
                + "FROM deitmo \n"
                + "JOIN item\n"
                + "ON item.itemID=deitmo.itemID\n"
                + "where deitmo.itemID = " + intemID;
        return aux;
    }

}
