/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.estructura;

import modelo.datos.ManejadorDB;
import modelo.negocio.Area;
import modelo.negocio.Color;
import modelo.negocio.Marca;
import modelo.negocio.Producto;
import modelo.negocio.Umedida;

/**
 *
 * @author Angy
 */
public class eProductoDetalle {

    private Integer productoID;
    private String nombre;
    private Integer precioV;
    private Integer stock;
    private Integer stockmin;
    private Integer stockmax;
    private String color;
    private String marca;
    private String area;
    private String umedida;

    public eProductoDetalle() {
    }

    public eProductoDetalle(Integer productoID, String nombre, Integer precioV, Integer stock, Integer stockmin, Integer stockmax, String color, String marca, String area, String umedida) {
        this.productoID = productoID;
        this.nombre = nombre;
        this.precioV = precioV;
        this.stock = stock;
        this.stockmin = stockmin;
        this.stockmax = stockmax;
        this.color = color;
        this.marca = marca;
        this.area = area;
        this.umedida = umedida;
    }

    public Integer getProductoID() {
        return productoID;
    }

    public void setProductoID(Integer productoID) {
        this.productoID = productoID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPrecioV() {
        return precioV;
    }

    public void setPrecioV(Integer precioV) {
        this.precioV = precioV;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getStockmin() {
        return stockmin;
    }

    public void setStockmin(Integer stockmin) {
        this.stockmin = stockmin;
    }

    public Integer getStockmax() {
        return stockmax;
    }

    public void setStockmax(Integer stockmax) {
        this.stockmax = stockmax;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public eProductoDetalle getDetalle(Producto p) {
        eProductoDetalle aux = new eProductoDetalle();
        aux.productoID = p.getProductoID();
        aux.nombre = p.getNombre();
        aux.precioV = p.getPrecioV();
        aux.stock = p.getStock();
        aux.stockmin = p.getStockmin();
        aux.stockmax = p.getStockmax();
        Color c = new Color();
        c.setColorID(p.getColorID());
        c = (Color) ManejadorDB.getManejador().getObjectId(c);
        aux.color = c.getDescripcion();
        Marca m = new Marca();
        m.setMarcaID(p.getMarcaID());
        m = (Marca) ManejadorDB.getManejador().getObjectId(m);
        aux.marca = m.getDescripcion();
        Area am = new Area();
        am.setAreaID(p.getAreaID());
        am = (Area) ManejadorDB.getManejador().getObjectId(am);
        aux.area = am.getDescripcion();
        Umedida um = new Umedida();
        um.setUmedidaID(p.getUmedidaID());
        um = (Umedida) ManejadorDB.getManejador().getObjectId(um);

        aux.umedida = um.getAbreviatura();
        return aux;
    }

}
