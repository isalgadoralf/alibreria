package modelo.negocio;

public class Umedida {

    private Integer umedidaID;
    private String abreviatura;
    private String descripcion;

    public Umedida( ) { 
      }
    public Umedida(Integer umedidaID,String abreviatura,String descripcion){
        this.umedidaID = umedidaID;
        this.abreviatura = abreviatura;
        this.descripcion = descripcion;
    }
    public Integer getUmedidaID() {
        return umedidaID;
    }

    public void setUmedidaID(Integer umedidaID) {
        this.umedidaID = umedidaID;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return this.getAbreviatura();
    }
    

}