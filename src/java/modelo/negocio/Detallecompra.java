package modelo.negocio;

public class Detallecompra {

    private Integer compraID;
    private Integer productoID;
    private Integer cantidad;
    private Integer precio;

    public Detallecompra( ) { 
      }
    public Detallecompra(Integer compraID,Integer productoID,Integer cantidad,Integer precio){
        this.compraID = compraID;
        this.productoID = productoID;
        this.cantidad = cantidad;
        this.precio = precio;
    }
    public Integer getCompraID() {
        return compraID;
    }

    public void setCompraID(Integer compraID) {
        this.compraID = compraID;
    }

    public Integer getProductoID() {
        return productoID;
    }

    public void setProductoID(Integer productoID) {
        this.productoID = productoID;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }


}