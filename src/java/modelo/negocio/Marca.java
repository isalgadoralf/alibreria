package modelo.negocio;

public class Marca {

    private Integer marcaID;
    private String descripcion;

    public Marca( ) { 
      }
    public Marca(Integer marcaID,String descripcion){
        this.marcaID = marcaID;
        this.descripcion = descripcion;
    }
    public Integer getMarcaID() {
        return marcaID;
    }

    public void setMarcaID(Integer marcaID) {
        this.marcaID = marcaID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return this.getDescripcion();
    }
    


}