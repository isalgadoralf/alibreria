package modelo.negocio;

public class Detalleventa {

    private Integer ventaID;
    private Integer productoID;
    private Integer cantidad;
    private Integer precio;

    public Detalleventa( ) { 
      }
    public Detalleventa(Integer ventaID,Integer productoID,Integer cantidad,Integer precio){
        this.ventaID = ventaID;
        this.productoID = productoID;
        this.cantidad = cantidad;
        this.precio = precio;
    }
    public Integer getVentaID() {
        return ventaID;
    }

    public void setVentaID(Integer ventaID) {
        this.ventaID = ventaID;
    }

    public Integer getProductoID() {
        return productoID;
    }

    public void setProductoID(Integer productoID) {
        this.productoID = productoID;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }


}