package modelo.negocio;

import java.util.Date;

public class Compra {

    private Integer compraID;
    private Integer descuento;
    private Date fecha;
    private Integer total;
    private Integer proveedorID;
    private Integer personalID;

    public Compra( ) { 
      }
    public Compra(Integer compraID,Integer descuento,Date fecha,Integer total,Integer proveedorID,Integer personalID){
        this.compraID = compraID;
        this.descuento = descuento;
        this.fecha = fecha;
        this.total = total;
        this.proveedorID = proveedorID;
        this.personalID = personalID;
    }
    public Integer getCompraID() {
        return compraID;
    }

    public void setCompraID(Integer compraID) {
        this.compraID = compraID;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getProveedorID() {
        return proveedorID;
    }

    public void setProveedorID(Integer proveedorID) {
        this.proveedorID = proveedorID;
    }

    public Integer getPersonalID() {
        return personalID;
    }

    public void setPersonalID(Integer personalID) {
        this.personalID = personalID;
    }


}