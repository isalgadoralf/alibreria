package modelo.negocio;

import java.util.Date;

public class Notasalida {

    private Integer notasalidaID;
    private Date fecha;
   

    public Notasalida( ) { 
      }
    public Notasalida(Integer notasalidaID,Date fecha){
        this.notasalidaID = notasalidaID;
        this.fecha = fecha;
     
    }
    public Integer getNotasalidaID() {
        return notasalidaID;
    }

    public void setNotasalidaID(Integer notasalidaID) {
        this.notasalidaID = notasalidaID;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    


}