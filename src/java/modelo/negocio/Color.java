package modelo.negocio;

public class Color {

    private Integer colorID;
    private String descripcion;

    public Color( ) { 
      }
    public Color(Integer colorID,String descripcion){
        this.colorID = colorID;
        this.descripcion = descripcion;
    }
    public Integer getColorID() {
        return colorID;
    }

    public void setColorID(Integer colorID) {
        this.colorID = colorID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return this.getDescripcion();
    }

    
}