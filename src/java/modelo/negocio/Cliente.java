package modelo.negocio;

public class Cliente {

    private Integer clienteID;
    private String carnet;
    private String nombre;
    private String telefono;

    public Cliente( ) { 
      }
    public Cliente(Integer clienteID,String carnet,String nombre,String telefono){
        this.clienteID = clienteID;
        this.carnet = carnet;
        this.nombre = nombre;
        this.telefono = telefono;
    }
    public Integer getClienteID() {
        return clienteID;
    }

    public void setClienteID(Integer clienteID) {
        this.clienteID = clienteID;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return this.getNombre();
    }

    
}