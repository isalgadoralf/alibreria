package modelo.negocio;

public class Personal {

    private Integer personalID;
    private String direccion;
    private String nombre;
    private String telefono;
    private String cargo;
    private String login;
    private String password;

    public Personal( ) { 
      }
    public Personal(Integer personalID,String direccion,String nombre,String telefono,String cargo,String login,String password){
        this.personalID = personalID;
        this.direccion = direccion;
        this.nombre = nombre;
        this.telefono = telefono;
        this.cargo = cargo;
        this.login = login;
        this.password = password;
    }
    public Integer getPersonalID() {
        return personalID;
    }

    public void setPersonalID(Integer personalID) {
        this.personalID = personalID;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}