package modelo.negocio;

public class Producto {

    private Integer productoID;
    private String nombre;
    private Integer precioV;
    private Integer stock;
    private Integer stockmin;
    private Integer stockmax;
    private Integer colorID;
    private Integer marcaID;
    private Integer areaID;
    private Integer umedidaID;

    public Producto( ) { 
      }
    public Producto(Integer productoID,String nombre,Integer precioV,Integer stock,Integer stockmin,Integer stockmax,Integer colorID,Integer marcaID,Integer areaID,Integer umedidaID){
        this.productoID = productoID;
        this.nombre = nombre;
        this.precioV = precioV;
        this.stock = stock;
        this.stockmin = stockmin;
        this.stockmax = stockmax;
        this.colorID = colorID;
        this.marcaID = marcaID;
        this.areaID = areaID;
        this.umedidaID = umedidaID;
    }
    public Integer getProductoID() {
        return productoID;
    }

    public void setProductoID(Integer productoID) {
        this.productoID = productoID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPrecioV() {
        return precioV;
    }

    public void setPrecioV(Integer precioV) {
        this.precioV = precioV;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getStockmin() {
        return stockmin;
    }

    public void setStockmin(Integer stockmin) {
        this.stockmin = stockmin;
    }

    public Integer getStockmax() {
        return stockmax;
    }

    public void setStockmax(Integer stockmax) {
        this.stockmax = stockmax;
    }

    public Integer getColorID() {
        return colorID;
    }

    public void setColorID(Integer colorID) {
        this.colorID = colorID;
    }

    public Integer getMarcaID() {
        return marcaID;
    }

    public void setMarcaID(Integer marcaID) {
        this.marcaID = marcaID;
    }

    public Integer getAreaID() {
        return areaID;
    }

    public void setAreaID(Integer areaID) {
        this.areaID = areaID;
    }

    public Integer getUmedidaID() {
        return umedidaID;
    }

    public void setUmedidaID(Integer umedidaID) {
        this.umedidaID = umedidaID;
    }

    @Override
    public String toString() {
        return  this.getNombre();
    }
    

}