package modelo.negocio;

import java.util.Date;

public class Venta {

    private Integer ventaID;
    private Integer descuento;
    private Date fecha;
    private Integer total;
    private Integer clienteID;
    private Integer personalID;

    public Venta( ) { 
      }
    public Venta(Integer ventaID,Integer descuento,Date fecha,Integer total,Integer clienteID,Integer personalID){
        this.ventaID = ventaID;
        this.descuento = descuento;
        this.fecha = fecha;
        this.total = total;
        this.clienteID = clienteID;
        this.personalID = personalID;
    }
    public Integer getVentaID() {
        return ventaID;
    }

    public void setVentaID(Integer ventaID) {
        this.ventaID = ventaID;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getClienteID() {
        return clienteID;
    }

    public void setClienteID(Integer clienteID) {
        this.clienteID = clienteID;
    }

    public Integer getPersonalID() {
        return personalID;
    }

    public void setPersonalID(Integer personalID) {
        this.personalID = personalID;
    }


}