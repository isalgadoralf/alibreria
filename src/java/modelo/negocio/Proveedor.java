package modelo.negocio;

public class Proveedor {

    private Integer proveedorID;
    private String direccion;
    private String nombre;
    private String telefono;

    public Proveedor( ) { 
      }
    public Proveedor(Integer proveedorID,String direccion,String nombre,String telefono){
        this.proveedorID = proveedorID;
        this.direccion = direccion;
        this.nombre = nombre;
        this.telefono = telefono;
    }
    public Integer getProveedorID() {
        return proveedorID;
    }

    public void setProveedorID(Integer proveedorID) {
        this.proveedorID = proveedorID;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return this.getNombre();
    }
    


}