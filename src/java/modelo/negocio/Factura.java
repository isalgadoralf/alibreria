package modelo.negocio;

import java.util.Date;

public class Factura {

    private Integer facturaID;
    private String nit;
    private Integer total;
    private Date fecha;
    private String detalle;
    private String nombre;
    private Integer ventaID;

    public Factura( ) { 
      }
    public Factura(Integer facturaID,String nit,Integer total,Date fecha,String detalle,String nombre,Integer ventaID){
        this.facturaID = facturaID;
        this.nit = nit;
        this.total = total;
        this.fecha = fecha;
        this.detalle = detalle;
        this.nombre = nombre;
        this.ventaID = ventaID;
    }
    public Integer getFacturaID() {
        return facturaID;
    }

    public void setFacturaID(Integer facturaID) {
        this.facturaID = facturaID;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getVentaID() {
        return ventaID;
    }

    public void setVentaID(Integer ventaID) {
        this.ventaID = ventaID;
    }


}