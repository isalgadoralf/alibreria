package modelo.negocio;

public class Detallesalida {

    private Integer notasalidaID;
    private Integer productoID;
    private Integer cantidad;
    private String observacion;
    private String _nombre;

    public Detallesalida( ) { 
      }
    public Detallesalida(Integer notasalidaID,Integer productoID,Integer cantidad,String observacion){
        this.notasalidaID = notasalidaID;
        this.productoID = productoID;
        this.cantidad = cantidad;
        this.observacion = observacion;
    }
    public Integer getNotasalidaID() {
        return notasalidaID;
    }

    public void setNotasalidaID(Integer notasalidaID) {
        this.notasalidaID = notasalidaID;
    }

    public Integer getProductoID() {
        return productoID;
    }

    public void setProductoID(Integer productoID) {
        this.productoID = productoID;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setNombre(String _nombre) {
        this._nombre = _nombre;
    }

    public String getNombre() {
        return _nombre;
    }

  
}