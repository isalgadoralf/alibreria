package modelo.negocio;

public class Area {

    private Integer areaID;
    private String descripcion;

    public Area( ) { 
      }
    public Area(Integer areaID,String descripcion){
        this.areaID = areaID;
        this.descripcion = descripcion;
    }
    public Integer getAreaID() {
        return areaID;
    }

    public void setAreaID(Integer areaID) {
        this.areaID = areaID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return this.getDescripcion();
    }
    


}