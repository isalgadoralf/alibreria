/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.negocio.Cliente;
import modelo.negocio.Proveedor;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bcliente {

    private Integer clienteID;
    private String carnet;
    private String nombre;
    private String telefono;
    private List<Cliente> clientes = new ArrayList<Cliente>();

    /**
     * Creates a new instance of bcliente
     */
    public bcliente() {
        cargarClientes();
    }

    public void cargarClientes() {
        clientes.clear();
        Cliente p = new Cliente();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Cliente element = (Cliente) datos.get(i);

            clientes.add(element);
        }
    }

    public void guardar(ActionEvent e) {
    //    Marca cat = categorias.get(categoria);
        //  Proveedores mm = new Proveedores(descripcion, Telefonos, ubicacion, cat.getCategoriaID(), Integer.MIN_VALUE);
        Cliente m = new Cliente(Integer.MIN_VALUE, carnet, nombre, telefono);
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
            cargarClientes();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public void modificar(ActionEvent e) {

        Cliente m = new Cliente(clienteID, carnet, nombre, telefono);
        int r = ManejadorDB.modificar(m);

        if (r > 0) {
            cargarClientes();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE MODIFICO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO MODIFICAR EL  PRODUCTO  " + " !"));
        }
    }

    public String onSeleccion(Cliente order) {
        this.clienteID = order.getClienteID();
        this.carnet = order.getCarnet();
        this.nombre = order.getNombre();
        this.telefono = order.getTelefono();
        return "";
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

   

}
