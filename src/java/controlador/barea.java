/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.negocio.Area;


/**
 *
 * @author Angy
 */
@ManagedBean
@ViewScoped
public class barea {
    private Integer areaID;
    private String descripcion;
    private List<Area> areas = new ArrayList<Area>();
    /**
     * Creates a new instance of barea
     */
    public barea() {
         cargarAreas();
    }
     public void cargarAreas() {
        areas.clear();
        Area p = new Area();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Area element = (Area) datos.get(i);
     
            areas.add(element);
        }
    }

    public void guardar(ActionEvent e) {
    //    Marca cat = categorias.get(categoria);
      //  Proveedores mm = new Proveedores(descripcion, Telefonos, ubicacion, cat.getCategoriaID(), Integer.MIN_VALUE);
          Area m = new Area(Integer.SIZE, this.getDescripcion());
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
            cargarAreas();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public void modificar(ActionEvent e) {
         
        Area m = new Area(this.areaID, this.getDescripcion());
        int r = ManejadorDB.modificar(m);

        if (r > 0) {
            cargarAreas();
         
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE MODIFICO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO MODIFICAR EL  PRODUCTO  " + " !"));
        }
    }
    public String onSeleccion(Area order) {
          this.descripcion = order.getDescripcion();
          this.areaID = order.getAreaID();
          
          System.out.println(""+descripcion +" "+areaID);
        return "";
    }

    public Integer getAreaID() {
        return areaID;
    }

    public void setAreaID(Integer areaID) {
        this.areaID = areaID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }
    
}
