/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;

import modelo.negocio.Factura;

import modelo.negocio.Venta;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bfactura {
  //   private Integer facturaID;
    private String nit;
    private Integer total;
    private Date fecha;
    private String detalle;
    private String nombre;
    private String venta;
    private Map<String, Venta> ventas = new HashMap<String, Venta>();
    private List<Factura> facturas = new ArrayList<Factura>();
    /**
     * Creates a new instance of bfactura
     */
    public bfactura() {
        cargarFacturas();
        cargarVenta();
    }
     public void cargarFacturas(){
        facturas.clear();
        Factura p = new Factura();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Factura element = (Factura) datos.get(i);
         
            facturas.add(element);
        }
        
    }
      public void cargarVenta(){
        ventas.clear();
        Venta p = new Venta();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Venta element = (Venta) datos.get(i);
         
            ventas.put(""+element.getVentaID(), element);
            
        }
        
    }
      public void guardar(ActionEvent e) {
        
         Venta au = ventas.get(venta);
         Factura m = new Factura(Integer.MIN_VALUE, nit, total, fecha, detalle, nombre, au.getVentaID());
         
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
                cargarFacturas();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVenta() {
        return venta;
    }

    public void setVenta(String venta) {
        this.venta = venta;
    }

   

    public Map<String, Venta> getVentas() {
        return ventas;
    }

    public void setVentas(Map<String, Venta> ventas) {
        this.ventas = ventas;
    }

    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }
    
    
}
