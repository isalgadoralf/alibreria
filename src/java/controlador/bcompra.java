/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.estructura.eProducto;
import modelo.negocio.Compra;
import modelo.negocio.Detallecompra;
import modelo.negocio.Detallesalida;
import modelo.negocio.Notasalida;
import modelo.negocio.Personal;
import modelo.negocio.Producto;
import modelo.negocio.Proveedor;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bcompra {

    //  private Integer compraID;
    private Integer descuento;
    private Date fecha;
    private Integer total;
    private String proveedor;
    private String producto;
    private Integer personalID;
    private Integer cantidad;
    private Integer precio;
    private Map<String, Proveedor> proveedores = new HashMap<String, Proveedor>();
    private Map<String, Producto> productos = new HashMap<String, Producto>();
    private List<eProducto> lista = new ArrayList<eProducto>();

    /**
     * Creates a new instance of bcompra
     */
    public bcompra() {
        cargarProveedor();
        cargarProducto();
        total  = 0;
    }

    private void cargarProveedor() {
        proveedores.clear();
        Proveedor p = new Proveedor();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Proveedor element = (Proveedor) datos.get(i);
            proveedores.put(element.getNombre(), element);
        }
    }

    private void cargarProducto() {
        productos.clear();
        Producto p = new Producto();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Producto element = (Producto) datos.get(i);
            productos.put(element.getNombre(), element);
        }
    }

    public void adicionarElemento(ActionEvent event) {

        Producto ma = productos.get(producto);
        int aux = precio * cantidad;
        eProducto e = new eProducto(ma.getProductoID(), ma.getNombre(), cantidad, precio, aux);
        total = total + aux;

        lista.add(e);

    }

    public void onRegitrar(ActionEvent e) {
        Personal u = (Personal) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
        Proveedor pro = proveedores.get(proveedor);
        Compra m = new Compra(Integer.MIN_VALUE, descuento, fecha, total, pro.getProveedorID(), u.getPersonalID());
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
         //   cargarColores();
             Compra ultimo = (Compra) ManejadorDB.getManejador().getUltimoObject(new Compra());
             guardarDetalle(ultimo.getCompraID());
             limpiar();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public String deleteAction(eProducto order) {

        boolean sw = true;
        int i = 0;
        while (sw && i < lista.size()) {
            eProducto e = lista.get(i);

            if (order.equals(e)) {
                Integer aux;
                aux = e.getPrecio() * e.getCantidad();

                total = total - aux;

                lista.remove(i);
                sw = !sw;
            }

            i++;
        }
        return null;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    public Integer getPersonalID() {
        return personalID;
    }

    public void setPersonalID(Integer personalID) {
        this.personalID = personalID;
    }

    public void setProveedores(Map<String, Proveedor> proveedores) {
        this.proveedores = proveedores;
    }

    public Map<String, Proveedor> getProveedores() {
        return proveedores;
    }

    public void setProductos(Map<String, Producto> productos) {
        this.productos = productos;
    }

    public Map<String, Producto> getProductos() {
        return productos;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setLista(List<eProducto> lista) {
        this.lista = lista;
    }

    public List<eProducto> getLista() {
        return lista;
    }

    private void guardarDetalle(Integer compraID) {
        for (int i = 0; i < lista.size(); i++) {
            eProducto d = lista.get(i);
            Detallecompra de = new Detallecompra(compraID, d.getProductoID(), d.getCantidad(), d.getPrecio());
            ManejadorDB.guardar(de);
        }
    }

    private void limpiar() {
        lista.clear();
    }

}
