/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.negocio.Marca;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bmarca  implements Serializable{
    private int marcaID;
    private String descripcion;
    private List<Marca> marcas = new ArrayList<Marca>();

    /**
     * Creates a new instance of bmarca
     */
    public bmarca() {
        cargarMarcas();
    }

    public void cargarMarcas() {
        marcas.clear();
        Marca p = new Marca();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Marca element = (Marca) datos.get(i);
     
            marcas.add(element);
        }
    }

    public void guardar(ActionEvent e) {
    //    Marca cat = categorias.get(categoria);
      //  Proveedores mm = new Proveedores(descripcion, Telefonos, ubicacion, cat.getCategoriaID(), Integer.MIN_VALUE);
          Marca m = new Marca(Integer.SIZE, this.getDescripcion());
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
            cargarMarcas();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public void modificar(ActionEvent e) {
         
        Marca m = new Marca(this.marcaID, this.getDescripcion());
        int r = ManejadorDB.modificar(m);

        if (r > 0) {
         cargarMarcas();
         
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE MODIFICO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO MODIFICAR EL  PRODUCTO  " + " !"));
        }
    }
    public String onSeleccion(Marca order) {
          this.descripcion = order.getDescripcion();
          this.marcaID = order.getMarcaID();
          
          System.out.println(""+descripcion +" "+marcaID);
        return "";
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

}
