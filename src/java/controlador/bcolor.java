/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.negocio.Color;
import modelo.negocio.Marca;

/**
 *
 * @author Angy
 */
@ManagedBean
@ViewScoped
public class bcolor {
    private Integer colorID;
    private String descripcion;
    private List<Color> colores = new ArrayList<Color>();
    /**
     * Creates a new instance of bcolor
     */
    public bcolor() {
        cargarColores();
    }
   
    public void cargarColores() {
        colores.clear();
        Color p = new Color();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Color element = (Color) datos.get(i);
     
            colores.add(element);
        }
    }

    public void guardar(ActionEvent e) {
    //    Marca cat = categorias.get(categoria);
      //  Proveedores mm = new Proveedores(descripcion, Telefonos, ubicacion, cat.getCategoriaID(), Integer.MIN_VALUE);
          Color m = new Color(Integer.SIZE, this.getDescripcion());
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
            cargarColores();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public void modificar(ActionEvent e) {
         
        Color m = new Color(this.colorID, this.getDescripcion());
        int r = ManejadorDB.modificar(m);

        if (r > 0) {
         cargarColores();
         
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE MODIFICO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO MODIFICAR EL  PRODUCTO  " + " !"));
        }
    }
    public String onSeleccion(Color order) {
          this.descripcion = order.getDescripcion();
          this.colorID = order.getColorID();
          
          System.out.println(""+descripcion +" "+colorID);
        return "";
    }

    public Integer getColorID() {
        return colorID;
    }

    public void setColorID(Integer colorID) {
        this.colorID = colorID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setColores(List<Color> colores) {
        this.colores = colores;
    }

    public List<Color> getColores() {
        return colores;
    }

    
    
    
}
