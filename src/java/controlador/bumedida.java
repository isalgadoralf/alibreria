/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.negocio.Area;
import modelo.negocio.Umedida;

/**
 *
 * @author Angy
 */
@ManagedBean
@ViewScoped
public class bumedida {
    private Integer umedidaID;
    private String abreviatura;
    private String descripcion;
    private List<Umedida> umedidas = new ArrayList<Umedida>();

    /**
     * Creates a new instance of bumedida
     */
    public bumedida() {
        cargarUmedidas();
    }
      public void cargarUmedidas() {
        umedidas.clear();
        Umedida p = new Umedida();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Umedida element = (Umedida) datos.get(i);
     
            umedidas.add(element);
        }
    }

    public void guardar(ActionEvent e) {
    //    Marca cat = categorias.get(categoria);
      //  Proveedores mm = new Proveedores(descripcion, Telefonos, ubicacion, cat.getCategoriaID(), Integer.MIN_VALUE);
          Umedida m = new Umedida(Integer.MIN_VALUE, abreviatura, descripcion);
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
            cargarUmedidas();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public void modificar(ActionEvent e) {
         
        Umedida m = new Umedida(umedidaID, abreviatura, descripcion);
        int r = ManejadorDB.modificar(m);

        if (r > 0) {
            cargarUmedidas();
         
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE MODIFICO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO MODIFICAR EL  PRODUCTO  " + " !"));
        }
    }
    public String onSeleccion(Umedida order) {
          this.descripcion = order.getDescripcion();
          this.umedidaID = order.getUmedidaID();
          this.abreviatura = order.getAbreviatura();
          
          System.out.println(""+descripcion +" "+umedidaID);
        return "";
    }

    public Integer getUmedidaID() {
        return umedidaID;
    }

    public void setUmedidaID(Integer umedidaID) {
        this.umedidaID = umedidaID;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Umedida> getUmedidas() {
        return umedidas;
    }

    public void setUmedidas(List<Umedida> umedidas) {
        this.umedidas = umedidas;
    }
    
    
}
