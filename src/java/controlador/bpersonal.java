/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.negocio.Personal;
import modelo.negocio.Proveedor;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bpersonal {

    private Integer personalID;
    private String direccion;
    private String nombre;
    private String telefono;
    private String cargo;
    private String login;
    private String password;
    private List<Personal> personales = new ArrayList<Personal>();

    /**
     * Creates a new instance of bpersonal
     */
    public bpersonal() {
        cargarPersonal();
    }

    public void cargarPersonal() {
        personales.clear();
        Personal p = new Personal();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Personal element = (Personal) datos.get(i);

            personales.add(element);
        }
    }

    public void guardar(ActionEvent e) {

        Personal m = new Personal(Integer.MIN_VALUE, direccion, nombre, telefono, cargo, login, password);
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
            cargarPersonal();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public void modificar(ActionEvent e) {

        Personal m = new Personal(personalID, direccion, nombre, telefono, cargo, login, password);
        int r = ManejadorDB.modificar(m);

        if (r > 0) {
            cargarPersonal();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE MODIFICO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO MODIFICAR EL  PRODUCTO  " + " !"));
        }
    }

    public String onSeleccion(Personal order) {
        this.personalID = order.getPersonalID();
        this.direccion = order.getDireccion();
        this.nombre = order.getNombre();
        this.telefono = order.getTelefono();
        this.cargo = order.getCargo();
        this.login = order.getLogin();
        this.password= order.getPassword();
        return "";
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPersonales(List<Personal> personales) {
        this.personales = personales;
    }

    public List<Personal> getPersonales() {
        return personales;
    }
    
}
