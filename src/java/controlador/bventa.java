/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.estructura.eProducto;
import modelo.negocio.Cliente;
import modelo.negocio.Compra;
import modelo.negocio.Detallecompra;
import modelo.negocio.Detalleventa;
import modelo.negocio.Personal;
import modelo.negocio.Producto;
import modelo.negocio.Proveedor;
import modelo.negocio.Venta;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bventa {

    private Integer descuento;
    private Date fecha;
    private Integer total;
    private String cliente;
    private String producto;
    private Integer cantidad;
    private Integer precio;
    private Map<String, Cliente> clientes = new HashMap<String, Cliente>();
    private Map<String, Producto> productos = new HashMap<String, Producto>();
    private List<eProducto> lista = new ArrayList<eProducto>();

    /**
     * Creates a new instance of bventa
     */
    public bventa() {
        cargarClientes();
        cargarProducto();
        total = 0;
    }
    private void cargarClientes() {
        clientes.clear();
        Cliente p = new Cliente();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Cliente element = (Cliente) datos.get(i);
            clientes.put(element.getNombre(), element);
        }
    }

    private void cargarProducto() {
        productos.clear();
        Producto p = new Producto();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Producto element = (Producto) datos.get(i);
            productos.put(element.getNombre(), element);
        }
    }

    public void adicionarElemento(ActionEvent event) {

        Producto ma = productos.get(producto);
        int aux = precio * cantidad;
        eProducto e = new eProducto(ma.getProductoID(), ma.getNombre(), cantidad, precio, aux);
        total = total + aux;

        lista.add(e);

    }

    public void onRegitrar(ActionEvent e) {
        Personal u = (Personal) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
        Cliente pro = clientes.get(cliente);
        Venta m = new Venta(Integer.MIN_VALUE, descuento, fecha, total, pro.getClienteID(), u.getPersonalID());
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
         //   cargarColores();
             Venta ultimo = (Venta) ManejadorDB.getManejador().getUltimoObject(new Venta());
             guardarDetalle(ultimo.getVentaID());
             limpiar();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public String deleteAction(eProducto order) {

        boolean sw = true;
        int i = 0;
        while (sw && i < lista.size()) {
            eProducto e = lista.get(i);

            if (order.equals(e)) {
                Integer aux;
                aux = e.getPrecio() * e.getCantidad();

                total = total - aux;

                lista.remove(i);
                sw = !sw;
            }

            i++;
        }
        return null;
    }

    private void guardarDetalle(Integer ventaID) {
        for (int i = 0; i < lista.size(); i++) {
            eProducto d = lista.get(i);
            Detalleventa de = new Detalleventa(ventaID, d.getProductoID(), d.getCantidad(), d.getPrecio());
            ManejadorDB.guardar(de);
        }
    }

    private void limpiar() {
        lista.clear();
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public Map<String, Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(Map<String, Cliente> clientes) {
        this.clientes = clientes;
    }

    public Map<String, Producto> getProductos() {
        return productos;
    }

    public void setProductos(Map<String, Producto> productos) {
        this.productos = productos;
    }

    public List<eProducto> getLista() {
        return lista;
    }

    public void setLista(List<eProducto> lista) {
        this.lista = lista;
    }
    
}
