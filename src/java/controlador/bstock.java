/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import modelo.datos.ManejadorDB;
import modelo.estructura.eProductoDetalle;
import modelo.negocio.Producto;

/**
 *
 * @author Rafael
 */
@ManagedBean
@RequestScoped
public class bstock {

    /**
     * Creates a new instance of bstock
     */
    private List<eProductoDetalle> productos = new ArrayList<eProductoDetalle>();

    public bstock() {
        cargarProductos();
    }

    public void setProductos(List<eProductoDetalle> productos) {
        this.productos = productos;
    }

    public List<eProductoDetalle> getProductos() {
        return productos;
    }
     public void cargarProductos(){
        productos.clear();
        Producto p = new Producto();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Producto element = (Producto) datos.get(i);
            eProductoDetalle aux = new eProductoDetalle();
            aux = aux.getDetalle(element);
            productos.add(aux);
        }
        
    }
}
