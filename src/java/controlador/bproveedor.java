/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;

import modelo.negocio.Proveedor;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bproveedor {

    private Integer proveedorID;
    private String direccion;
    private String nombre;
    private String telefono;
    private List<Proveedor> proveedores = new ArrayList<Proveedor>();

    /**
     * Creates a new instance of bproveedor
     */
    public bproveedor() {
        cargarProveedor();
    }
    public void cargarProveedor() {
        proveedores.clear();
        Proveedor p = new Proveedor();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Proveedor element = (Proveedor) datos.get(i);
     
            proveedores.add(element);
        }
    }

    public void guardar(ActionEvent e) {
    //    Marca cat = categorias.get(categoria);
      //  Proveedores mm = new Proveedores(descripcion, Telefonos, ubicacion, cat.getCategoriaID(), Integer.MIN_VALUE);
        Proveedor m = new Proveedor(Integer.MIN_VALUE, direccion, nombre, telefono);
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
            cargarProveedor();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public void modificar(ActionEvent e) {
         
         Proveedor m = new Proveedor(proveedorID, direccion, nombre, telefono);
        int r = ManejadorDB.modificar(m);

        if (r > 0) {
          cargarProveedor();
         
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE MODIFICO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO MODIFICAR EL  PRODUCTO  " + " !"));
        }
    }
    public String onSeleccion(Proveedor order) {
           this.proveedorID = order.getProveedorID();
        this.direccion = order.getDireccion();
        this.nombre = order.getNombre();
        this.telefono = order.getTelefono();
        return "";
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setProveedores(List<Proveedor> proveedores) {
        this.proveedores = proveedores;
    }

    public List<Proveedor> getProveedores() {
        return proveedores;
    }
    
}
