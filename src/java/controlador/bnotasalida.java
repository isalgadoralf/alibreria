/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.estructura.eProducto;
import modelo.negocio.Detallesalida;
import modelo.negocio.Notasalida;
import modelo.negocio.Producto;

/**
 *
 * @author Angy
 */
@ManagedBean
@ViewScoped
public class bnotasalida {

    private Integer notasalidaID;
    private Date fecha;
  
    private Integer productoID;
    private Integer cantidad;
    private String observacion;
    private String producto;
    private Map<String, Producto> productos = new HashMap<String, Producto>();
    private List<Detallesalida> lista = new ArrayList<Detallesalida>();

    /**
     * Creates a new instance of bnotasalida
     */
    public bnotasalida() {
        cargarProducto();
    }
     private void cargarProducto() {
        productos.clear();
        Producto p = new Producto();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Producto element = (Producto) datos.get(i);
            productos.put(element.getNombre(), element);
        }
    }
     public void adicionarElemento(ActionEvent event) {

        Producto ma = productos.get(producto);
        Detallesalida e = new Detallesalida(Integer.MIN_VALUE, ma.getProductoID(), cantidad, observacion);
        e.setNombre(ma.getNombre());
        lista.add(e);
       //  System.out.println(lista.size());

    }

    public void onRegitrar(ActionEvent e) {
        Notasalida m = new Notasalida(Integer.MIN_VALUE, fecha);
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
         //   cargarColores();
             Notasalida ultimo = (Notasalida) ManejadorDB.getManejador().getUltimoObject(new Notasalida());
             guardarDetalle(ultimo.getNotasalidaID());
             limpiar();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public String deleteAction(Detallesalida order) {

        boolean sw = true;
        int i = 0;
        while (sw && i < lista.size()) {
            Detallesalida e = lista.get(i);

            if (order.equals(e)) {
                

                lista.remove(i);
                sw = !sw;
            }

            i++;
        }
        return null;
    }
    private void guardarDetalle(int notaID){
     
        for (int i = 0; i < lista.size(); i++) {
            Detallesalida de = lista.get(i);
            de.setNotasalidaID(notaID);
          
            ManejadorDB.guardar(de);
        }
    }
    private void limpiar(){
        lista.clear();
    }
       

    public Integer getNotasalidaID() {
        return notasalidaID;
    }

    public void setNotasalidaID(Integer notasalidaID) {
        this.notasalidaID = notasalidaID;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

   

    public Integer getProductoID() {
        return productoID;
    }

    public void setProductoID(Integer productoID) {
        this.productoID = productoID;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Map<String, Producto> getProductos() {
        return productos;
    }

    public void setProductos(Map<String, Producto> productos) {
        this.productos = productos;
    }

    public List<Detallesalida> getLista() {
        return lista;
    }

    public void setLista(List<Detallesalida> lista) {
        this.lista = lista;
    }
    
}
