/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import modelo.datos.ManejadorDB;
import modelo.estructura.eProductoDetalle;
import modelo.negocio.Area;
import modelo.negocio.Color;
import modelo.negocio.Marca;
import modelo.negocio.Producto;
import modelo.negocio.Umedida;

/**
 *
 * @author Angy
 */
@ManagedBean
@ViewScoped
public class bproducto {
    private Integer productoID;
    private String nombre;
    private Integer precioV;
    private Integer stock;
    private Integer stockmin;
    private Integer stockmax;
    private String color;
    private String marca;
    private String area;
    private String umedida;
    
     private Map<String, Color> colores = new HashMap<String, Color>();
     private Map<String, Area> areas = new HashMap<String, Area>();
     private Map<String, Marca> marcas = new HashMap<String, Marca>();
     private Map<String, Umedida> umedidas = new HashMap<String, Umedida>();
     
      private List<eProductoDetalle> productos = new ArrayList<eProductoDetalle>();
    
    /**
     * Creates a new instance of bproducto
     */
    public bproducto() {
        iniciar();
        
    }
    
     public void guardar(ActionEvent e) {
         Color ac = colores.get(color);
         Area aa = areas.get(area);
         Marca am = marcas.get(marca);
         Umedida au = umedidas.get(umedida);
         Producto m = new Producto(1, nombre, precioV, stock, stockmin, stockmax, ac.getColorID(), am.getMarcaID(), aa.getAreaID(), au.getUmedidaID());
         
        int r = ManejadorDB.guardar(m);

        if (r > 0) {
            cargarProductos();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }
    }

    public void modificar(ActionEvent e) {
         Color ac = colores.get(color);
         Area aa = areas.get(area);
         Marca am = marcas.get(marca);
         Umedida au = umedidas.get(umedida);
         
         Producto m = new Producto(productoID, nombre, precioV, stock, stockmin, stockmax, ac.getColorID(), am.getMarcaID(), aa.getAreaID(), au.getUmedidaID());
        int r = ManejadorDB.modificar(m);

        if (r > 0) {
            cargarProductos();
         
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE MODIFICO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO MODIFICAR EL  PRODUCTO  " + " !"));
        }
    }
    public String onSeleccion(eProductoDetalle order) {
         this.productoID = order.getProductoID();
        this.nombre = order.getNombre();
        this.precioV = order.getPrecioV();
        this.stock = order.getStock();
        this.stockmin = order.getStockmin();
        this.stockmax = order.getStockmax();
        this.color = order.getColor();
        this.marca = order.getMarca();
        this.area = order.getArea();
        this.umedida = order.getUmedida();
          
        
        return "";
    }
    public void cargarProductos(){
        productos.clear();
        Producto p = new Producto();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Producto element = (Producto) datos.get(i);
            eProductoDetalle aux = new eProductoDetalle();
            aux = aux.getDetalle(element);
            productos.add(aux);
        }
        
    }
    public void cargarArea(){
        areas.clear();
        Area p = new Area();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Area element = (Area) datos.get(i);
         
            areas.put(element.getDescripcion(), element);
            
        }
        
    }
    public void cargarColor(){
        colores.clear();
        Color p = new Color();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Color element = (Color) datos.get(i);
         
            colores.put(element.getDescripcion(), element);
            
        }
        
    }
    public void cargarMarca(){
           marcas.clear();
        Marca p = new Marca();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Marca element = (Marca) datos.get(i);
         
            marcas.put(element.getDescripcion(), element);
            
        }
        
    }
    public void cargarUmedida(){
          umedidas.clear();
        Umedida p = new Umedida();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Umedida element = (Umedida) datos.get(i);
         
            umedidas.put(element.getAbreviatura(), element);
            
        }
    }
    public void iniciar(){
        cargarArea();
        cargarColor();
        cargarMarca();
        cargarUmedida();
        cargarProductos();
        
    }

    public Integer getProductoID() {
        return productoID;
    }

    public void setProductoID(Integer productoID) {
        this.productoID = productoID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPrecioV() {
        return precioV;
    }

    public void setPrecioV(Integer precioV) {
        this.precioV = precioV;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getStockmin() {
        return stockmin;
    }

    public void setStockmin(Integer stockmin) {
        this.stockmin = stockmin;
    }

    public Integer getStockmax() {
        return stockmax;
    }

    public void setStockmax(Integer stockmax) {
        this.stockmax = stockmax;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public Map<String, Color> getColores() {
        return colores;
    }

    public void setColores(Map<String, Color> colores) {
        this.colores = colores;
    }

    public Map<String, Area> getAreas() {
        return areas;
    }

    public void setAreas(Map<String, Area> areas) {
        this.areas = areas;
    }

    public Map<String, Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(Map<String, Marca> marcas) {
        this.marcas = marcas;
    }

    public Map<String, Umedida> getUmedidas() {
        return umedidas;
    }

    public void setUmedidas(Map<String, Umedida> umedidas) {
        this.umedidas = umedidas;
    }

    public List<eProductoDetalle> getProductos() {
        return productos;
    }

    public void setProductos(List<eProductoDetalle> productos) {
        this.productos = productos;
    }
    
    
}
